import React, { Component } from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import "../../css/style.css";
const DemoCarousel = () => {
    return (
        <div className="carousel-container">
            <Carousel autoPlay interval={3000} infiniteLoop showThumbs={false}>
                <div>
                    <img
                        src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/df319a104344643.5f60f7ffcb5b2.jpg"
                        style={{ width: "100%", height: "100%" }}
                    />
                </div>
                <div>
                    <img
                        src="https://www.basarbazar.com.bd/wp-content/uploads/2019/12/Banner1.jpg"
                        style={{ width: "100%", height: "100%" }}
                    />
                </div>
                <div>
                    <img
                        src="https://shebanin.com/storage/photos/shares/SERVICES%20LAST/RlyM8ieNotEt72WZqhoSi99Deq1PVKJkOaOy77Fb.jpeg"
                        style={{ width: "100%", height: "100%" }}
                    />
                </div>
            </Carousel>
        </div>
    );
};
export default DemoCarousel;
