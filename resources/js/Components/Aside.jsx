import { Link } from "@inertiajs/react";
import {
    Category,
    Dashboard,
    DinnerDiningRounded,
    Icecream,
    Inventory2TwoTone,
    LocalDrinkSharp,
    SetMeal,
    Signpost,
} from "@mui/icons-material";
import React from "react";

const Aside = () => {
    return (
        <div>
            <aside
                id="sidebar-multi-level-sidebar"
                className="fixed top-0 left-0 z-20 flex flex-col flex-shrink-0  w-64 h-full pt-16 font-normal duration-75 lg:flex transition-width"
                aria-label="Sidebar"
            >
                <div className="h-full px-3 py-4 overflow-y-auto bg-slate-200 dark:bg-gray-800">
                    <ul className="space-y-2 font-medium">
                        <li>
                            <Link
                                href="/dashboard"
                                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group"
                            >
                                <Dashboard />
                                <span className="ml-3">Dashboard</span>
                            </Link>
                        </li>

                        <li>
                            <Link
                                href="/posts"
                                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group"
                            >
                                <Signpost />
                                <span className="flex-1 ml-3 whitespace-nowrap">
                                    Posts
                                </span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                href="/categories"
                                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group"
                            >
                                <Category />
                                <span className="flex-1 ml-3 whitespace-nowrap">
                                    Categories
                                </span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                href="/products"
                                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group"
                            >
                                <Inventory2TwoTone />
                                <span className="flex-1 ml-3 whitespace-nowrap">
                                    Products
                                </span>
                            </Link>
                        </li>

                        <li>
                            <Link
                                href="/"
                                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group"
                            >
                                <LocalDrinkSharp />
                                <span className="flex-1 ml-3 whitespace-nowrap">
                                    Drinks
                                </span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                href="/"
                                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group"
                            >
                                <DinnerDiningRounded />
                                <span className="flex-1 ml-3 whitespace-nowrap">
                                    Lunch Items
                                </span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                href="/"
                                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group"
                            >
                                <Icecream />
                                <span className="flex-1 ml-3 whitespace-nowrap">
                                    Snacks
                                </span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                href="/"
                                className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group"
                            >
                                <SetMeal />
                                <span className="flex-1 ml-3 whitespace-nowrap">
                                    Dinner
                                </span>
                            </Link>
                        </li>
                    </ul>
                </div>
            </aside>
        </div>
    );
};

export default Aside;
