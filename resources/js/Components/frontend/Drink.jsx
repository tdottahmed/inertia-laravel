import React, { useState } from "react";
import {
    AddShoppingCartOutlined,
    ExpandMoreOutlined,
} from "@mui/icons-material";
import { Rating, Stack } from "@mui/material";
import { Link } from "@inertiajs/react";

const Drink = ({ products, storageUrl }) => {
    const maxProductsToShow = 6;
    const [productsToShow, setProductsToShow] = useState(maxProductsToShow);

    const snackProducts = products
        .filter((product) => product.category.title === "Drinks")
        .slice(0, productsToShow);

    const loadMore = () => {
        setProductsToShow(productsToShow + maxProductsToShow);
    };

    return (
        <div>
            <div className="bg-slate-100">
                <h1 className="mb-4 text-3xl font-extrabold text-center py-3 text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
                    <span className="text-transparent bg-clip-text bg-gradient-to-r to-emerald-600 from-sky-400">
                        Special Drinks
                    </span>
                </h1>
                <div className="max-w-screen-xl flex flex-wrap items-center mx-auto p-4">
                    {snackProducts.map((product, index) => (
                        <div
                            key={index}
                            className="w-full sm:w-1/2 md:w-1/3 lg:w-1/3 xl:w-1/3 mb-4 p-2"
                        >
                            <div className="bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 w-full">
                                <a href="#">
                                    <img
                                        className="p-8 rounded-t-lg"
                                        src={`${storageUrl}/products/${product.image}`}
                                        alt="product image"
                                        height={"100%"}
                                        width={"100%"}
                                    />
                                </a>
                                <div className="px-5 pb-5">
                                    <a href="#">
                                        <h5 className="text-xl font-semibold tracking-tight text-gray-900 dark:text-white">
                                            {product.title}
                                        </h5>
                                        <p>
                                            {product.description.length > 15
                                                ? product.description.substring(
                                                      0,
                                                      15
                                                  ) + "..."
                                                : product.description}
                                        </p>
                                    </a>
                                    <Stack spacing={1}>
                                        <Rating
                                            name="half-rating-read"
                                            defaultValue={2.5}
                                            precision={0.5}
                                            readOnly
                                        />
                                    </Stack>
                                    <div className="flex items-center justify-between">
                                        <span className="text-3xl font-bold text-gray-900 dark:text-white">
                                            BDT:-{product.price}Tk/-
                                        </span>
                                        <Link
                                            href={route(
                                                "home.placeOrder",
                                                product.id
                                            )}
                                            className="text-xs text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 shadow-lg shadow-purple-500/50 dark:shadow-lg dark:shadow-purple-800/80 font-medium rounded-lg px-5 py-2.5 text-center mr-2 mb-2"
                                        >
                                            <AddShoppingCartOutlined /> Order
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
                {productsToShow < products.length && (
                    <div className="text-center">
                        <button
                            className="relative inline-flex items-center justify-center p-0.5 mb-2 mr-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-purple-600 to-blue-500 group-hover:from-purple-600 group-hover:to-blue-500 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800"
                            onClick={loadMore}
                        >
                            <span className="relative px-5 py-2.5 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0">
                                Load More <ExpandMoreOutlined />
                            </span>
                        </button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default Drink;
