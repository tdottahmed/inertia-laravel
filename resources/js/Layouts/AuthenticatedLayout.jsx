import Aside from "@/Components/Aside";
import Header from "@/Components/Header";

export default function Authenticated({ children }) {
    return (
        <div className="min-h-screen bg-gray-100 mt-8">
            <Header />
            <Aside />
            <div className="p-4 sm:ml-64 my-16">
                <div className="p-4 border-2 border-gray-200 border-dashed rounded-lg dark:border-gray-700">
                    <main>{children}</main>
                </div>
            </div>
        </div>
    );
}
