import React from "react";
import MUIDataTable from "mui-datatables";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, useForm } from "@inertiajs/react";
import { Delete, Edit, Visibility } from "@mui/icons-material";

export default function index({ auth, products, imageUrl }) {
    const { delete: destroy } = useForm();

    function deleteBtnClick(id) {
        if (confirm("Are you sure you want to delete this product?")) {
            destroy(route("products.destroy", id));
        }
    }

    const columns = [
        {
            name: "Ser No",
        },
        {
            name: "Title",
        },
        {
            name: "Category",
        },
        {
            name: "Description",
        },
        {
            name: "Image",
            options: {
                customBodyRender: (value) => (
                    <img
                        src={`${imageUrl}/${value}`}
                        alt=""
                        height={"150px"}
                        width={"150px"}
                    />
                ),
            },
        },
        {
            name: "Price",
        },
        {
            name: "Action",
            options: {
                customBodyRender: (value, tableMeta, updateValue) => (
                    <div>
                        <Link
                            href={route(
                                "products.edit",
                                products[tableMeta.rowIndex].id
                            )}
                        >
                            <Edit />
                        </Link>
                        <Link
                            href={route(
                                "products.show",
                                products[tableMeta.rowIndex].id
                            )}
                        >
                            <Visibility />
                        </Link>
                        <button
                            onClick={() =>
                                deleteBtnClick(products[tableMeta.rowIndex].id)
                            }
                        >
                            <Delete />
                        </button>
                    </div>
                ),
            },
        },
    ];

    const data = products.map((product, index) => [
        index + 1,
        product.title,
        product.category.title,
        product.description,
        product.image, // You might need to format this for your needs
        product.price,
    ]);

    const options = {
        filterType: "textField", // Add a search input
        responsive: "standard",
        print: true,
        download: true,
        selectableRows: "none",
        pagination: true,
    };

    return (
        <AuthenticatedLayout user={auth.user}>
            <h1 className="mb-8 text-3xl font-bold text-center">
                Products List
            </h1>
            <div className="flex items-center justify-between mb-6">
                <Link
                    className="px-6 py-2 text-white bg-teal-800 rounded-md focus:outline-none"
                    href={route("products.create")}
                >
                    Create product
                </Link>
            </div>
            <MUIDataTable
                title={"Products List"}
                data={data}
                columns={columns}
                options={options}
            />
        </AuthenticatedLayout>
    );
}
