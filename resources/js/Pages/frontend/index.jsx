import Carousel from "@/Components/Carousel";
import Dinner from "@/Components/frontend/Dinner";
import Drink from "@/Components/frontend/Drink";
import Footer from "@/Components/frontend/Footer";
import Snacks from "@/Components/frontend/Snacks";
import Lunch from "@/Components/frontend/lunch";
import Nav from "@/Components/frontend/nav";

export default function index({ storageUrl, products, auth }) {
    return (
        <>
            <Nav storageUrl={storageUrl} auth={auth} /> 
            <Carousel />
            <Lunch products={products} storageUrl={storageUrl} />
            <Snacks products={products} storageUrl={storageUrl} />
            <Drink products={products} storageUrl={storageUrl} />
            <Dinner products={products} storageUrl={storageUrl} />
            <Footer storageUrl={storageUrl} />
        </>
    );
}
