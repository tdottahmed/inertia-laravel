import Footer from "@/Components/frontend/Footer";
import Nav from "@/Components/frontend/nav";
import { useForm } from "@inertiajs/react";
import React from "react";

function Order({ storageUrl, product, auth }) {
    const { setData, data, errors, post } = useForm({
        email: auth.user.email || "",
        name: auth.user.name || "",
        product: product.id,
        contact: auth.user.phone || "",
        house: "",
        Road: "",
        area: "",
        unit_price: product.price || 0,
        qty: 1,
        total: 0,
    });

    const incrementQuantity = () => {
        setData("qty", data.qty + 1);
    };

    const decrementQuantity = () => {
        if (data.qty > 1) {
            setData("qty", data.qty - 1);
        }
    };

    function handleSubmit(e) {
        e.preventDefault();
        post(route("order.payment"));
    }

    return (
        <>
            <Nav storageUrl={storageUrl} />
            <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
                <div className="p-4">
                    <div className="w-full bg-white border border-gray-200 shadow">
                        <img
                            className="w-full h-auto rounded-t-lg"
                            src={`${storageUrl}/products/${product.image}`}
                            alt="Product Image"
                        />
                        <div className="p-4 leading-normal">
                            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                {product.title}
                            </h5>
                            <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                                {product.description}
                            </p>
                        </div>
                    </div>
                </div>
                <div className="p-4">
                    <div className="w-full bg-white border border-gray-200 shadow px-4 py-7">
                        <form onSubmit={handleSubmit}>
                            <div className="mb-6">
                                <label
                                    htmlFor="email"
                                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Your Email
                                </label>
                                <input
                                    name="email"
                                    type="email"
                                    id="email"
                                    value={data.email}
                                    onChange={(e) =>
                                        setData("email", e.target.value)
                                    }
                                    className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                                    placeholder="name@flowbite.com"
                                    required
                                />
                            </div>
                            <div className="mb-6">
                                <label
                                    htmlFor="name"
                                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Your Name
                                </label>
                                <input
                                    name="name"
                                    type="text"
                                    id="name"
                                    value={data.name}
                                    onChange={(e) =>
                                        setData("name", e.target.value)
                                    }
                                    className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                                    required
                                />
                            </div>
                            <div className="mb-6">
                                <label
                                    htmlFor="contact"
                                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Phone No.
                                </label>
                                <input
                                    name="contact"
                                    type="text"
                                    id="contact"
                                    value={data.contact}
                                    onChange={(e) =>
                                        setData("contact", e.target.value)
                                    }
                                    className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                                    required
                                />
                            </div>

                            <div className="mb-6 flex flex-col sm:flex-row justify-between gap-1">
                                <div className="w-full sm:w-1/2">
                                    <label
                                        htmlFor="house"
                                        className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                    >
                                        House No.
                                    </label>
                                    <input
                                        name="house"
                                        type="number"
                                        id="house"
                                        value={data.house}
                                        onChange={(e) =>
                                            setData("house", e.target.value)
                                        }
                                        className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                                        required
                                    />
                                </div>
                                <div className="w-full sm:w-1/2">
                                    <label
                                        htmlFor="Road"
                                        className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                    >
                                        Road No.
                                    </label>
                                    <input
                                        name="Road"
                                        type="number"
                                        id="Road"
                                        value={data.Road}
                                        onChange={(e) =>
                                            setData("Road", e.target.value)
                                        }
                                        className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                                        required
                                    />
                                </div>
                            </div>
                            <div className="mb-6">
                                <label
                                    htmlFor="area"
                                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Area Details.
                                </label>
                                <input
                                    name="area"
                                    type="text"
                                    id="area"
                                    value={data.area}
                                    onChange={(e) =>
                                        setData("area", e.target.value)
                                    }
                                    placeholder="Ex..Sector No./ commercial area etc."
                                    className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                                    required
                                />
                            </div>

                            <div className="mb-6">
                                <label
                                    htmlFor="unit_price"
                                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Unit Price
                                </label>
                                <input
                                    name="unit_price"
                                    type="text"
                                    id="unit_price"
                                    value={data.unit_price}
                                    onChange={(e) =>
                                        setData("unit_price", e.target.value)
                                    }
                                    disabled
                                    className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                                    required
                                />
                            </div>
                            <div className="mb-6 flex flex-col sm:flex-row items-center justify-between gap-1">
                                <div className="w-full sm:w-1/2">
                                    <label
                                        htmlFor="unit_price"
                                        className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                    >
                                        Select Quantity
                                    </label>
                                    <div className="flex items-center">
                                        <button
                                            type="button"
                                            onClick={decrementQuantity}
                                            className="px-3 py-2 text-gray-700 border rounded-l cursor-pointer focus:outline-none"
                                        >
                                            -
                                        </button>
                                        <input
                                            name="quantity"
                                            type="number"
                                            id="quantity"
                                            value={data.qty}
                                            readOnly
                                            className="w-24 text-center border border-gray-300 focus:outline-none"
                                            required
                                        />
                                        <button
                                            type="button"
                                            onClick={incrementQuantity}
                                            className="px-3 py-2 text-gray-700 border rounded-r cursor-pointer focus:outline-none"
                                        >
                                            +
                                        </button>
                                    </div>
                                </div>
                                <div className="w-full sm:w-1/2 mt-2 sm:mt-0">
                                    <label
                                        htmlFor="unit_price"
                                        className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                    >
                                        Total
                                    </label>
                                    <input
                                        name="quantity"
                                        type="number"
                                        id="quantity"
                                        value={data.qty * data.unit_price}
                                        readOnly
                                        className="w-full text-center border border-gray-300 focus:outline-none"
                                        required
                                    />
                                </div>
                            </div>

                            <button
                                type="submit"
                                className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover-bg-blue-700 dark:focus:ring-blue-800"
                            >
                                Order Now
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <Footer storageUrl={storageUrl} />
        </>
    );
}

export default Order;
