<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function index()
    {
        $products = Product::with('category')->get();
        $storageUrl = asset("storage");
        return Inertia::render('frontend/index',['storageUrl'=>$storageUrl, 'products'=>$products]);
    }
     public function placeOrder(Product $product)
     {
        $storageUrl = asset("storage");
        return Inertia::render('frontend/Order', ["product"=>$product, 'storageUrl'=>$storageUrl]);
        
     }
}
