<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $imageUrl = asset("storage/products/");
        $products = Product::with('category')->get();
        return Inertia::render('backend/product/index', ['products'=>$products,'imageUrl' => $imageUrl]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::all();
        return Inertia::render('backend/product/create', ['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        
        $image = $this->uploadImage($request->image);
        try {
            Product::create([
                "category_id"=>$request->category,
                'title'=>$request->title,
                'description'=>$request->description,
                'price'=>$request->price,
                'image'=>$image

            ]);
            return redirect()->route('products.index')->with('msg','success');
        } catch (\Illuminate\Database\QueryException $e) {
            Log::error("Database Error: " . $e->getMessage());
            return redirect()->back()->with('error', 'An error occurred while saving the data. Please try again later.');
        } catch (\Exception $e) {
            Log::error("Error: " . $e->getMessage());
            return redirect()->back()->with('error', 'An unexpected error occurred. Please contact support.');
        }
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        $imageUrl = asset("storage/products/");
        return Inertia::render('backend/product/show', ['product'=>$product, 'imageUrl'=>$imageUrl]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        $imageUrl = asset("storage/products/");
        $categories = Category::all();
        return Inertia::render('backend/product/edit', ['product'=>$product, 'categories'=>$categories, 'imageUrl'=>$imageUrl]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {        
        try {
            if ($request->hasFile('image')) {
                $image = $this->uploadImage($request->image);
            } else {
                $image = $product->image;
            }
            $product->update([
                "category_id"=>$request->category,
                'title'=>$request->title,
                'description'=>$request->description,
                'price'=>$request->price,
                'image'=>$image

            ]);
            return redirect()->route('products.index')->with('msg','success');
        } 
        
        catch (\Illuminate\Database\QueryException $e) {
            Log::error("Database Error: " . $e->getMessage());
            return redirect()->back()->with('error', 'An error occurred while saving the data. Please try again later.');
        } catch (\Exception $e) {
            Log::error("Error: " . $e->getMessage());
            return redirect()->back()->with('error', 'An unexpected error occurred. Please contact support.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $product->delete();

    }

    public function uploadImage($file)
 {
        $fileName= time().'.'.$file->getClientOriginalExtension();
        Image::make($file)
        ->resize(150,150)
        ->save(storage_path().'/app/public/products/'. $fileName);
        return $fileName;

 }

}
